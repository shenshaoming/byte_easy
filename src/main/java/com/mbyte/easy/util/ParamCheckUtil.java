package com.mbyte.easy.util;

/**
 * 请求参数检验所有的工具类
 *
 * @author SSM
 * @date 2020/5/6 9:23
 */
public class ParamCheckUtil {
    public static boolean isLimitLength(String str, int min, int max){
        if (str == null){
            return false;
        }
        if (str.length() <= max && str.length() >= min){
            return true;
        } else {
            return false;
        }
    }
}
