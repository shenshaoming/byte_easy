package com.mbyte.easy.rest.comment;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.user.entity.Comment;
import com.mbyte.easy.user.service.ICommentService;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.util.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
* <p>
* 前端控制器
* </p>
* @author SSM
* @since 2020-5-6
*/
@RestController
@RequestMapping("rest/comment")
public class RestCommentController extends BaseController  {

    @Autowired
    private ICommentService commentService;

    /**
     * 通过视频id获取该视频下的评论信息
     * @param map {videoId 视频id(必要参数), lastId 可选参数}
     * @return
     */
    @PostMapping("getComments")
    public AjaxResult getComments(@RequestBody HashMap<String, String> map){
        String videoId = map.get("videoId");
        if (videoId == null){
            return paramError();
        }
        QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
        queryWrapper = queryWrapper.eq("video_id",videoId);
        String lastId = map.get("lastId");
        if (lastId != null){
            queryWrapper = queryWrapper.gt("t_video.id", lastId);
        }

        // 每一次搜出来10条信息
        int limit = 10;
        Page<Comment> page = new Page<>(1, limit);
        return success(commentService.page(page, queryWrapper).getRecords());
    }

    /**
    * 查询列表
    *
    * @param pageNo
    * @param pageSize
    * @param comment
    * @return
    */
    @RequestMapping
    public AjaxResult index(@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize, String createTimeSpace, Comment comment) {
        Page<Comment> page = new Page<Comment>(pageNo, pageSize);
        QueryWrapper<Comment> queryWrapper = new QueryWrapper<Comment>();

        if(comment.getUserId() != null  && !"".equals(comment.getUserId() + "")) {
            queryWrapper = queryWrapper.like("user_id",comment.getUserId());
         }


        if(comment.getGoodCount() != null  && !"".equals(comment.getGoodCount() + "")) {
            queryWrapper = queryWrapper.like("good_count",comment.getGoodCount());
         }

        IPage<Comment> pageInfo = commentService.page(page, queryWrapper);

        Map<String, Object> map = new HashMap<>();
        map.put("searchInfo",  comment);
        map.put("pageInfo", new PageInfo(pageInfo));

        return this.success(map);
    }


    /**
    * 添加
    * @param comment
    * @return
    */
    @PostMapping("add")
    public AjaxResult add(@RequestBody Comment comment){
        comment.setCreateTime(LocalDateTime.now());
        return toAjax(commentService.save(comment));
    }

    /**
    * 添加
    * @param comment
    * @return
    */
    @PostMapping("edit")
    public AjaxResult edit(Comment comment){
        return toAjax(commentService.updateById(comment));
    }
    /**
    * 删除
    * @param id
    * @return
    */
    @GetMapping("delete/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        return toAjax(commentService.removeById(id));
    }
    /**
    * 批量删除
    * @param ids
    * @return
    */
    @PostMapping("deleteAll")
    public AjaxResult deleteAll(@RequestBody List<Long> ids){
        return toAjax(commentService.removeByIds(ids));
    }

}

