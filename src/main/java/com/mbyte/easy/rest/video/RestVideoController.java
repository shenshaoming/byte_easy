package com.mbyte.easy.rest.video;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.util.FileUtil;
import com.mbyte.easy.video.entity.Video;
import com.mbyte.easy.video.service.IVideoService;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.util.PageInfo;
import com.mbyte.easy.video.service.IVideoTypeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
* <p>
* 前端控制器
* </p>
* @author SSM
* @since 2020-5-6
*/
@RestController
@RequestMapping("rest/video")
public class RestVideoController extends BaseController  {

    @Autowired
    private IVideoService videoService;

    @Autowired
    private IVideoTypeService videoTypeService;

    /**
     * @Description : 上传视频
     * 用户上传视频后,可以对该上传添加描述信息等，将描述信息和视频分开，
     * 即简单又能够不漏下任何一个文件
     *
     * @param file 视频文件
     * @param userId 用户id
     * @Return : com.mbyte.easy.common.web.AjaxResult
     * @Author : SSM
     * @Date : 2020/5/6 15:47
    */
	@RequestMapping("upload")
	public AjaxResult upload(@RequestParam("file") MultipartFile file,
                             @RequestParam("userId") Long userId
                             ){
        String filePath = FileUtil.uploadFile(file);

        // 在视频表中添加记录
        Video video = new Video();
        video.setCreateTime(LocalDateTime.now());
        video.setUpdateTime(LocalDateTime.now());
        video.setUserId(userId);
        video.setVideoPath(filePath);
        videoService.save(video);
        JSONObject data = new JSONObject();
        data.put("videoId", video.getId());
        data.put("videoPath", video.getVideoPath());
        return success(data);
	}

	/**
	 * @Description : 获取视频访问top10
	 *
	 * @Return : com.mbyte.easy.common.web.AjaxResult
	 * @Author : 申劭明
	 * @Date : 2020/5/6 17:10
	*/
	@RequestMapping("rankingList")
    public AjaxResult rankingList(){
	    QueryWrapper<Video> queryWrapper = new QueryWrapper<>();
	    queryWrapper = queryWrapper.orderByDesc("point_count");
	    queryWrapper = queryWrapper.eq("status", 1);
	    Page<Video> page = new Page<>(1, 10);
	    return success(new PageInfo<>(videoService.page(page, queryWrapper)));
    }

    @RequestMapping("addCount")
    public AjaxResult addCount(@RequestBody Video video){
        videoService.addCount(video.getId());
        return success();
	}

    /**
     * 随意刷视频
     * @param video  视频对象()
     * @return
     */
	@PostMapping("getVideos")
    public AjaxResult getVideos(@RequestBody Video video){
	    QueryWrapper<Video> queryWrapper = new QueryWrapper<>();
	    queryWrapper = queryWrapper.gt("t_video.id", video.getId())
                .eq("status", 1);

	    // 每次显示5个视频
	    int limit = 5;

	    Page<Video> page = new Page<>(1, limit);
	    return success(videoService.page(page, queryWrapper).getRecords());
    }

    /**
     * @Description : 获取用户个人的视频
     * 该接口懒加载
     *
     * @Return : com.mbyte.easy.common.web.AjaxResult
     * @Author : 申劭明
     * @Date : 2020/5/6 17:10
     */
	@RequestMapping("userVideo")
    public AjaxResult userVideo(@RequestBody HashMap<String, String> paramMap){
        // 用户上一次加载到的视频
	    String lastId = paramMap.get("lastId");
	    // 用户的id
	    String userId = paramMap.get("userId");

	    if (userId == null){
	        return paramError();
        }

	    QueryWrapper<Video> queryWrapper = new QueryWrapper<>();
	    queryWrapper = queryWrapper.eq("user_id", userId);
	    if (lastId != null){
	        queryWrapper = queryWrapper.gt("t_video.id", lastId);
        }
        int limit = 2;
        Page<Video> page = new Page<>(1,limit);
        return success(videoService.page(page, queryWrapper).getRecords());
    }

    @PostMapping("typeList")
    public AjaxResult typeList(){
	    return success(videoTypeService.list());
    }

    /**
    * 查询列表
    *
    * @param pageNo
    * @param pageSize
    * @param video
    * @return
    */
    @RequestMapping
    public AjaxResult index(@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize, String createTimeSpace, String updateTimeSpace, Video video) {
        Page<Video> page = new Page<>(pageNo, pageSize);
        QueryWrapper<Video> queryWrapper = new QueryWrapper<>();

        if(video.getUserId() != null  && !"".equals(video.getUserId() + "")) {
            queryWrapper = queryWrapper.like("user_id",video.getUserId());
         }


        if(video.getTypeId() != null  && !"".equals(video.getTypeId() + "")) {
            queryWrapper = queryWrapper.like("type_id",video.getTypeId());
         }


        if(video.getVideoPath() != null  && !"".equals(video.getVideoPath() + "")) {
            queryWrapper = queryWrapper.like("video_path",video.getVideoPath());
         }


        if(video.getStatus() != null  && !"".equals(video.getStatus() + "")) {
            queryWrapper = queryWrapper.like("status",video.getStatus());
         }


        if(video.getDescription() != null  && !"".equals(video.getDescription() + "")) {
            queryWrapper = queryWrapper.like("description",video.getDescription());
         }


        if(video.getPointCount() != null  && !"".equals(video.getPointCount() + "")) {
            queryWrapper = queryWrapper.like("point_count",video.getPointCount());
         }

        IPage<Video> pageInfo = videoService.page(page, queryWrapper);

        Map<String, Object> map = new HashMap<>();
        map.put("searchInfo",  video);
        map.put("pageInfo", new PageInfo(pageInfo));

        return this.success(map);
    }

    /**
    * 修改
    * @param video 只能根据主键id修改描述信息等
    * @return
    */
    @PostMapping("edit")
    public AjaxResult edit(@RequestBody Video video){
        video.setUpdateTime(LocalDateTime.now());
        return toAjax(videoService.updateById(video));
    }

    /**
    * 用户只能删除自己上传的视频
    * @param video 只需要主键id和用户id就可以了
    * @return
    */
    @PostMapping("delete")
    public AjaxResult delete(@RequestBody Video video){
        if (video.getUserId() == null){
            return paramError();
        }
        return toAjax(videoService.removeById(video.getUserId(), video.getId()));
    }
    /**
    * 批量删除
    * @param ids
    * @return
    */
    @PostMapping("deleteAll")
    public AjaxResult deleteAll(@RequestBody List<Long> ids){
        return toAjax(videoService.removeByIds(ids));
    }

}

