package com.mbyte.easy.rest.user;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.security.entity.SysUser;
import com.mbyte.easy.security.service.IRoleService;
import com.mbyte.easy.security.service.IUserService;
import com.mbyte.easy.util.FileUtil;
import com.mbyte.easy.util.TokenProccessor;
import com.mbyte.easy.util.Utility;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashSet;
import java.util.Map;
import java.util.Random;

/**
 * @author SSM
 * @date 2020/5/5 22:57
 */
@RestController
@RequestMapping("rest/user")
public class RestUserController extends BaseController {

    private final IUserService userService;

    private final IRoleService roleService;

    /**
     * 用于存储生成的token的Set集合
     */
    private static final HashSet<String> TOKEN_SET = new HashSet<>();

    public RestUserController(IUserService userService, IRoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }

    @RequestMapping("login")
    public AjaxResult login(@RequestBody SysUser user){
        if (StringUtils.isEmpty(user.getUsername()) || StringUtils.isEmpty(user.getPassword())) {
            return paramError();
        }
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
        queryWrapper = queryWrapper.eq("username", user.getUsername());
        SysUser one = userService.getOne(queryWrapper);
        if (one != null){
            if (Utility.checkPassword(user.getPassword(), one.getPassword())){
                one.setRoles(roleService.selectRolesByUserId(one.getId()));
                if ("普通用户".equals(one.getRoles().get(0).getName())){
                    String token = TokenProccessor.getInstance().makeToken();
                    TOKEN_SET.add(token);
                    one.setPassword(null);
                    JSONObject data = new JSONObject();
                    data.put("userInfo", one);
                    data.put("token", token);
                    return success(data);
                }
            }
        }
        return error("账号或密码错误");
    }

    /**
     * @Description 注册用户
     *
     * @param user 需要有用户名和密码
     * @return com.mbyte.easy.common.web.AjaxResult
     * @author SSM
     * @date 2020/5/5 23:43
    */
    @RequestMapping("register")
    public AjaxResult register(@RequestBody SysUser user){
        if (StringUtils.isEmpty(user.getUsername())
                || StringUtils.isEmpty(user.getPassword())) {
            return paramError();
        }

        user.setPassword(Utility.QuickPassword(user.getPassword()));

        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
        queryWrapper = queryWrapper.eq("username", user.getUsername());
        SysUser one = userService.getOne(queryWrapper);
        if (one != null){
            return paramError("该用户已注册");
        }
        user.setNickname("智视" + new Random().nextInt(1000));
        userService.register(user);
        String token = TokenProccessor.getInstance().makeToken();
        TOKEN_SET.add(token);
        user.setPassword(null);
        JSONObject data = new JSONObject();
        data.put("userInfo", user);
        data.put("token", token);
        return success(data);
    }

    @PostMapping("edit")
    public AjaxResult edit(@RequestBody SysUser sysUser){
        userService.updateById(sysUser);
        SysUser resultUser = userService.getById(sysUser.getId());
        resultUser.setPassword(null);
        return success(resultUser);
    }

    @PostMapping("uploadImg")
    public AjaxResult uploadImg(@RequestParam MultipartFile file, @RequestParam Long userId){
        String filePath = FileUtil.uploadFile(file);
        SysUser user = new SysUser();
        user.setId(userId);
        user.setHeadImage(filePath);
        userService.updateById(user);
        SysUser resultUser = userService.getById(user.getId());
        resultUser.setPassword(null);
        return success(resultUser);
    }

    @PostMapping("logout")
    public AjaxResult logout(@RequestBody Map<String, String> paramMap){
        String userId = paramMap.get("userId");
        String token = paramMap.get("token");

        if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(token)){
            return paramError();
        }

        TOKEN_SET.remove(token);
        return success();
    }
}
