package com.mbyte.easy.user.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.user.entity.Comment;
import com.mbyte.easy.user.service.ICommentService;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.util.PageInfo;
import com.mbyte.easy.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.util.ObjectUtils;
import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
* <p>
* 前端控制器
* </p>
* @author SSM
* @since 2020-5-6
*/
@Controller
@RequestMapping("/user/comment")
public class CommentController extends BaseController  {

    private String prefix = "user/comment/";

    @Autowired
    private ICommentService commentService;

    /**
    * 查询列表
    *
    * @param model
    * @param pageNo
    * @param pageSize
    * @param comment
    * @return
    */
    @RequestMapping
    public String index(Model model,@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,
                        @RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize
                , @ModelAttribute("createTimeSpace")String createTimeSpace        , Comment comment) {
        Page<Comment> page = new Page<>(pageNo, pageSize);
        QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
            if(!ObjectUtils.isEmpty(comment.getUserId())) {
                queryWrapper = queryWrapper.like("user_id",comment.getUserId());
            }
            if (!ObjectUtils.isEmpty(createTimeSpace)){
                try {
                    LocalDateTime[] localDateTimes = DateUtil.stringToLocals(createTimeSpace);
                    queryWrapper = queryWrapper.between("create_time",localDateTimes[0],localDateTimes[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if(!ObjectUtils.isEmpty(comment.getGoodCount())) {
                queryWrapper = queryWrapper.like("good_count",comment.getGoodCount());
            }
        IPage<Comment> pageInfo = commentService.page(page, queryWrapper);
        model.addAttribute("searchInfo", comment);
        model.addAttribute("pageInfo", new PageInfo(pageInfo));
        return prefix+"list";
    }

    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("addBefore")
    public String addBefore(){
        return prefix+"add";
    }
    /**
    * 添加
    * @param comment
    * @return
    */
    @PostMapping("add")
    @ResponseBody
    public AjaxResult add(Comment comment){
            LocalDateTime createTime = LocalDateTime.now();
            comment.setCreateTime(createTime);
        return toAjax(commentService.save(comment));
    }
    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("editBefore/{id}")
    public String editBefore(Model model,@PathVariable("id")Long id){
        model.addAttribute("comment",commentService.getById(id));
        return prefix+"edit";
    }
    /**
    * 添加
    * @param comment
    * @return
    */
    @PostMapping("edit")
    @ResponseBody
    public AjaxResult edit(Comment comment){
        return toAjax(commentService.updateById(comment));
    }
    /**
    * 删除
    * @param id
    * @return
    */
    @GetMapping("delete/{id}")
    @ResponseBody
    public AjaxResult delete(@PathVariable("id") Long id){
        return toAjax(commentService.removeById(id));
    }
    /**
    * 批量删除
    * @param ids
    * @return
    */
    @PostMapping("deleteAll")
    @ResponseBody
    public AjaxResult deleteAll(@RequestBody List<Long> ids){
        return toAjax(commentService.removeByIds(ids));
    }

}

