package com.mbyte.easy.user.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.mbyte.easy.user.entity.Comment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author SSM
 * @since 2020-05-06
 */
public interface CommentMapper extends BaseMapper<Comment> {


    IPage<Comment> page(@Param("page") IPage<Comment> page, @Param(Constants.WRAPPER) Wrapper<Comment> queryWrapper);
}
