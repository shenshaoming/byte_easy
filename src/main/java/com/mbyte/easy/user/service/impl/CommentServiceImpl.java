package com.mbyte.easy.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mbyte.easy.user.entity.Comment;
import com.mbyte.easy.user.mapper.CommentMapper;
import com.mbyte.easy.user.service.ICommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mbyte.easy.video.mapper.VideoMapper;
import jdk.nashorn.internal.ir.annotations.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author SSM
 * @since 2020-05-06
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements ICommentService {

    @Autowired
    private VideoMapper videoMapper;

    @Override
    public IPage<Comment> page(IPage<Comment> page, Wrapper<Comment> queryWrapper) {
        return this.baseMapper.page(page, queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean save(Comment entity) {
        videoMapper.addCommentCount(entity.getVideoId());
        return super.save(entity);
    }
}
