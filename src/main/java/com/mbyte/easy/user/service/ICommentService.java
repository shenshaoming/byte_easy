package com.mbyte.easy.user.service;

import com.mbyte.easy.user.entity.Comment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author SSM
 * @since 2020-05-06
 */
public interface ICommentService extends IService<Comment> {

}
