package com.mbyte.easy.video.service.impl;

import com.mbyte.easy.video.entity.VideoType;
import com.mbyte.easy.video.mapper.VideoTypeMapper;
import com.mbyte.easy.video.service.IVideoTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 视频类型字典表 服务实现类
 * </p>
 *
 * @author SSM
 * @since 2020-05-06
 */
@Service
public class VideoTypeServiceImpl extends ServiceImpl<VideoTypeMapper, VideoType> implements IVideoTypeService {

}
