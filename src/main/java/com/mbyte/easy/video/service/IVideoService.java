package com.mbyte.easy.video.service;

import com.mbyte.easy.video.entity.Video;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 视频表 服务类
 * </p>
 *
 * @author SSM
 * @since 2020-05-06
 */
public interface IVideoService extends IService<Video> {

    /**
     * @Description : 根据<code>videoId</code>删除数据，但是需要通过用户id进行校验
     * 避免别的用户删除属于该用户的视频
     *
     * @param userId 用户id
     * @param videoId 视频id
     * @Return : int 删除操作影响的行数, 1或0
     * @Author : SSM
     * @Date : 2020/5/6 15:55
    */
    int removeById(long userId, long videoId);

    /**
     * 将id对应视频的点击量加1
     * @param id
     */
    void addCount(Long id);

    /**
     * 用户的视频列表
     * @param lastId 上一次搜索到的id(可以为null)
     * @param limit 限制数量
     * @param userId 当前登录用户的id(不可为null)
     * @return
     */
    List<Video> userVideo(String lastId, int limit, String userId);
}
