package com.mbyte.easy.video.service;

import com.mbyte.easy.video.entity.VideoType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 视频类型字典表 服务类
 * </p>
 *
 * @author SSM
 * @since 2020-05-06
 */
public interface IVideoTypeService extends IService<VideoType> {

}
