package com.mbyte.easy.video.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mbyte.easy.util.FileUtil;
import com.mbyte.easy.video.entity.Video;
import com.mbyte.easy.video.mapper.VideoMapper;
import com.mbyte.easy.video.service.IVideoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;

/**
 * <p>
 * 视频表 服务实现类
 * </p>
 *
 * @author SSM
 * @since 2020-05-06
 */
@Service
@Slf4j
public class VideoServiceImpl extends ServiceImpl<VideoMapper, Video> implements IVideoService {

    @Override
    public IPage<Video> page(IPage<Video> page, Wrapper<Video> queryWrapper) {
        return page.setRecords(this.baseMapper.page(page, queryWrapper));
    }

    @Override
    public int removeById(long userId, long videoId) {
        QueryWrapper<Video> queryWrapper = new QueryWrapper<>();
        queryWrapper = queryWrapper.eq("user_id", userId)
                .eq("id", videoId);
        Video video = this.baseMapper.selectOne(queryWrapper);
        if (video == null){
            return 0;
        }
        // 现在磁盘上删除文件
        String localPath = FileUtil.getLocalPath(video.getVideoPath());
        System.err.println("删除文件: " + localPath);
        File file = new File(localPath);
        if (file.exists()){
            file.delete();
        }

        return this.baseMapper.delete(queryWrapper);
    }

    @Override
    public void addCount(Long id) {
        this.baseMapper.addCount(id);
    }

    @Override
    public List<Video> userVideo(String lastId, int limit, String userId) {
        return this.baseMapper.userVideoList(lastId, limit, userId);
    }
}
