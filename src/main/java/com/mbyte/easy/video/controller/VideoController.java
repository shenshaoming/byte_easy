package com.mbyte.easy.video.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.util.FileUtil;
import com.mbyte.easy.video.entity.Video;
import com.mbyte.easy.video.service.IVideoService;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.util.PageInfo;
import com.mbyte.easy.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author SSM
 * @since 2020-5-6
 */
@Controller
@RequestMapping("/video/video")
public class VideoController extends BaseController {

    private String prefix = "video/video/";

    @Autowired
    private IVideoService videoService;

    /**
     * 查询列表
     *
     * @param model
     * @param pageNo
     * @param pageSize
     * @param video
     * @return
     */
    @RequestMapping
    public String index(Model model, @RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,
                        @RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize
            , @ModelAttribute("createTimeSpace") String createTimeSpace, @ModelAttribute("updateTimeSpace") String updateTimeSpace, Video video) {
        Page<Video> page = new Page<>(pageNo, pageSize);
        QueryWrapper<Video> queryWrapper = new QueryWrapper<>();
        if (!ObjectUtils.isEmpty(video.getUserId())) {
            queryWrapper = queryWrapper.like("user_id", video.getUserId());
        }
        if (!ObjectUtils.isEmpty(video.getTypeId())) {
            queryWrapper = queryWrapper.like("type_id", video.getTypeId());
        }
        if (!ObjectUtils.isEmpty(video.getVideoPath())) {
            queryWrapper = queryWrapper.like("video_path", video.getVideoPath());
        }
        if (!ObjectUtils.isEmpty(createTimeSpace)) {
            try {
                LocalDateTime[] localDateTimes = DateUtil.stringToLocals(createTimeSpace);
                queryWrapper = queryWrapper.between("create_time", localDateTimes[0], localDateTimes[1]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (!ObjectUtils.isEmpty(updateTimeSpace)) {
            try {
                LocalDateTime[] localDateTimes = DateUtil.stringToLocals(updateTimeSpace);
                queryWrapper = queryWrapper.between("update_time", localDateTimes[0], localDateTimes[1]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (!ObjectUtils.isEmpty(video.getStatus())) {
            queryWrapper = queryWrapper.like("status", video.getStatus());
        }
        if (!ObjectUtils.isEmpty(video.getDescription())) {
            queryWrapper = queryWrapper.like("description", video.getDescription());
        }
        if (!ObjectUtils.isEmpty(video.getPointCount())) {
            queryWrapper = queryWrapper.like("point_count", video.getPointCount());
        }
        queryWrapper = queryWrapper.orderByAsc("status");
        IPage<Video> pageInfo = videoService.page(page, queryWrapper);
        model.addAttribute("searchInfo", video);
        model.addAttribute("pageInfo", new PageInfo(pageInfo));
        return prefix + "list";
    }

    /**
     * 添加跳转页面
     *
     * @return
     */
    @GetMapping("addBefore")
    public String addBefore() {
        return prefix + "add";
    }

    /**
     * 添加
     *
     * @param video
     * @return
     */
    @PostMapping("add")
    @ResponseBody
    public AjaxResult add(Video video) {
        LocalDateTime createTime = LocalDateTime.now();
        video.setCreateTime(createTime);
        LocalDateTime updateTime = LocalDateTime.now();
        video.setUpdateTime(updateTime);
        return toAjax(videoService.save(video));
    }

    /**
     * 添加跳转页面
     *
     * @return
     */
    @GetMapping("editBefore/{id}")
    public String editBefore(Model model, @PathVariable("id") Long id) {
        model.addAttribute("video", videoService.getById(id));
        return prefix + "edit";
    }

    /**
     * 修改
     *
     * @param video
     * @return
     */
    @PostMapping("edit")
    @ResponseBody
    public AjaxResult edit(Video video) {
        LocalDateTime updateTime = LocalDateTime.now();
        video.setUpdateTime(updateTime);
        return toAjax(videoService.updateById(video));
    }

    /**
     * 删除,删除只会删除文件,不会删除数据库中的记录
     *
     * @param id
     * @return
     */
    @GetMapping("delete/{id}")
    @ResponseBody
    public AjaxResult delete(@PathVariable("id") Long id) {
        Video video = videoService.getById(id);
        if (video == null){
            return paramError();
        }
        String localPath = FileUtil.getLocalPath(video.getVideoPath());
        File file = new File(localPath);
        if (file.exists()){
            file.delete();
            return success("删除成功");
        } else {
            return error("删除失败，文件不存在");
        }
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @PostMapping("deleteAll")
    @ResponseBody
    public AjaxResult deleteAll(@RequestBody List<Long> ids) {
        return toAjax(videoService.removeByIds(ids));
    }

}

