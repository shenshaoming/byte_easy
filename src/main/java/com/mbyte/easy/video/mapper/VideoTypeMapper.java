package com.mbyte.easy.video.mapper;

import com.mbyte.easy.video.entity.VideoType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 视频类型字典表 Mapper 接口
 * </p>
 *
 * @author SSM
 * @since 2020-05-06
 */
public interface VideoTypeMapper extends BaseMapper<VideoType> {

}
