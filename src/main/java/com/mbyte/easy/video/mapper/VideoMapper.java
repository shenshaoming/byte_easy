package com.mbyte.easy.video.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.mbyte.easy.video.entity.Video;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 * 视频表 Mapper 接口
 * </p>
 *
 * @author SSM
 * @since 2020-05-06
 */
public interface VideoMapper extends BaseMapper<Video> {


    List<Video> page(@Param("page") IPage<Video> page,
                     @Param(Constants.WRAPPER) Wrapper<Video> queryWrapper);

    /**
     * @Description 视频点击量加1
     *
     * @param id 主键
     * @return void
     * @author SSM
     * @date 2020/5/7 0:18
    */
    @Update("UPDATE t_video SET point_count=point_count+1 WHERE id = #{id}")
    void addCount(@Param("id") Long id);

    /**
     * @Description 视频点击量加1
     *
     * @param lastId 上一次搜索的id(可以为null)
     * @param limit 限制
     * @param userId 用户id
     * @return List
     * @author SSM
     * @date 2020/5/7 0:18
     */
    List<Video> userVideoList(String lastId, int limit, String userId);

    @Update("UPDATE t_video SET comment_count=comment_count+1 WHERE id = #{id}")
    void addCommentCount(@Param("id") Long videoId);
}
