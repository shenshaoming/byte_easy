package com.mbyte.easy.video.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.mbyte.easy.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 视频类型字典表
 * </p>
 *
 * @author SSM
 * @since 2020-05-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("dict_video_type")
public class VideoType extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 视频类型名称
     */
    private String name;

    /**
     * 类型描述
     */
    private String description;


}
