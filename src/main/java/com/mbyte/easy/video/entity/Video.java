package com.mbyte.easy.video.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mbyte.easy.common.entity.BaseEntity;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 视频表
 * </p>
 *
 * @author SSM
 * @since 2020-05-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("t_video")
public class Video extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 上传用户的id
     */
    private Long userId;

    /**
     * 用户昵称
     */
    @TableField(exist = false)
    private String nickname;

    /**
     * 用户头像
     */
    @TableField(exist = false)
    private String headImage;

    /**
     * 视频类型id
     */
    private Long typeId;

    /**
     * 视频类型
     */
    @TableField(exist = false)
    private String videoType;

    /**
     * 上传视频路径
     */
    private String videoPath;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 是否过审(0为未过审,1为过审,2为违规)
     */
    private Integer status;

    /**
     * 视频描述
     */
    private String description;

    /**
     * 点击量
     */
    private Long pointCount;

    /**
     * 评论数
     */
    private Long commentCount;
}
