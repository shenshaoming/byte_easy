package com.mbyte.easy;

import junit.framework.TestCase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * @auther SSM
 * @date 19-4-11 20:28
 * @deprecated : 测试空工具类
 */
public class TestEmpty extends TestCase {

    @Test
    public static void main(String [] args){
        System.out.println("/fileSuffix/20200507/20200507232244544_4S0YOO.jpg".length());
    }

    private static class MyThredIteratorThread <T> extends Thread{

        private final Spliterator<T> list;

        private MyThredIteratorThread(Spliterator<T> list) {
            this.list = list;
        }

        @Override
        public void run() {
            list.forEachRemaining(e -> System.out.println(e));

            list.forEachRemaining(new Consumer<T>() {
                @Override
                public void accept(T t) {
                    System.out.println(t);
                }
            });
        }
    }

}
